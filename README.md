[![Unity Engine](https://img.shields.io/badge/unity-2022.2.8f1-black.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)

# Better_Skybox

Following tutorial [How to get rid of Unity default skybox look ! Unity Tutorial](https://www.youtube.com/watch?v=f6zUot73-gg&ab_channel=AlexStrook).

![screenshot](etc/screenshot.png)

## 🧑‍🏫 Curriculum

- Skybox
- Env. lighting
- Env. reflections
- Post-processing
